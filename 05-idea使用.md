# idea 使用



## 1. 安装方式

* 打开 idea，选择 File - settings - plugins,或使用快捷键 ctrl + alt + s 打开 settings 
* 在弹出的窗口左上角可以输入插件关键字，搜索、并安装，点击 install 安装
* 安装完后，重启编译器即可

## 2. 常用插件

### 2.1 阿里java 语法规范

* 名称：Alibaba Java Coding Guidelines
* 

### 2.2 Key promoter

* idea 快捷键提示

### 2.3 Lombok

* 代码简化插件