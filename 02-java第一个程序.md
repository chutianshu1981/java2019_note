# 第一个 Java程序

## 1. 你好世界！

``` java
public class Hello{
    public static void main(String[] args){
        System.out.println("世界，你好！");
    }
}

```

## 2. 类代码编写注意

* 类是一个完整代码逻辑机构，其中有功能和数据，可以描述万事万物，是组成java程序的基本逻辑单位。
* 使用 public class 关键字定义，类体代码用 {} 表示开始和结束
* 类名使用驼峰camel命名法，每个单词首字母大写，其他字母小写
* 类名可以随意取，但必须符合命名规则：只能包含英文的大小写字母，_ 以及阿拉伯数字，并且数字不能是第一位

## 3. 编译执行

``` shell
#将源代码编译成可执行的java 字节码
javac Hello.java
# 执行后，若无错误，会在相同路径下生成一个同名的 .class 文件
# 使用 java 命令，执行 Java程序
java Hello.class
```
