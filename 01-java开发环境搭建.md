# 01 - java 开发环境搭建

## 1. 安装配置 JDK

* 下载安装 jdk 11 64k 版本
* 安装完后，需要进行配置：打开我的电脑-右键点“计算机”-属性-高级-选“环境变量”-新建系统变量
* 变量名输入：JAVA_HOME  变量值输入jdk的安装路径，如“C:\Program Files\Java\jdk-11.0.2”，点确定完成操作
* 然后，需要编辑 Path 环境变量值，添加新的执行路径，操作如下：
   双击找到的 Path 变量，在变量值后添加 ;C:\Program Files\Java\jdk-11.0.2\bin 然后点击确定完成操作
* 测试是否成功：win+r 打开运行，输入 cmd 敲回车"enter"，打开命令行，输入 java -version ,如果成功，会显示版本号

## 2. 安装编译器 intellj idea

* 双击安装 ideaIU-2019.2.1.exe ，安装成功后重启
* 用试用模式进入软件，在激活界面选择 evaluate for free 
* 在欢迎界面，选择 configure （配置）- edit  custom VM options ，在打开的窗口最后一行，输入你存放破解文件的路径
* 在我的电脑上，破解文件存放位置是 D:\setup，所以我的输入为 -javaagent:D:\setup\JetbrainsCrack.jar 然后点击 save 保存并关闭
* 继续可以正常打开编译器，在菜单中选择 Help - register - 选择激活方式为 lessens server，在下面输入 http://jetbrains-license-server ,然后点 activate 激活即可

## 3. 关注课堂笔记项目
* 注册 gitee.com
* 右上角搜索 java2019_note 项目
* 点 fork 即可

## 4. 安装 vscode 和 git

> 注意，必须以管理员权限安装

* 先双击安装 vscode
* 后双击安装 git

## 5. 使用 git 克隆项目

* 打开我的电脑，找到一个想要存放项目的路径，在空白处右键，选择“ Git Bash here”，会弹出 git 的命令行
* 在命令行中使用 git clone 项目地址 的命令，就可以将云端项目克隆到本地

``` shell
git clone https://gitee.com/chutianshu1981/java2019_note.git
```

## 6. 在 vs code 中编辑并提交项目

* 打开 vs code, 在菜单栏中选择 File - open folder ，选择已经克隆到本地的项目文件夹，打开
* 在 vs code 中编辑项目，保存后并提交
* 项目一经改变，会在 source control 中，显示更新
* 然后，在 Message 中随便输入一些信息作为本次更新的说明，现阶段尽可能简单即可，然后点击上面的对号进行提交 commit，这阶段只是将其更新入代码仓库，并没有上传
* 最后，点击同步按钮 sync ，将代码同步上云
* 如果第一次上云，需要绑定完云项目账号信息，就可以将更新提交到云中+

``` shell
#
git config --global user.email "xxxx@yyy.com"
git config --global user.password "xxxxxx"
```

## 7. ftp 服务器

ftp://192.168.3.2/

## 8. baidu云

hnlyjava2019
