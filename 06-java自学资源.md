# java 自学资源

1. 官方文档

* 11 版英文：https://docs.oracle.com/en/java/javase/11/
* 8 版本中文：https://blog.fondme.cn/apidoc/jdk-1.8-google/

2. 第三方入门教程

* 廖雪峰：https://www.liaoxuefeng.com/wiki/1252599548343744
* 菜鸟：https://www.runoob.com/java/java-tutorial.html
* w3school:https://www.w3cschool.cn/java/

3. java 知乎学习路线

* https://www.zhihu.com/question/307096748/answer/677936196

4. Github awesome-java

* https://github.com/jobbole/awesome-java-cn
* https://github.com/akullpp/awesome-java

5. idea 文档

https://www.w3cschool.cn/intellij_idea_doc/intellij_idea_doc-q3ke2coy.html

6. javaFx

* https://www.w3cschool.cn/java/javafx-listview.html
* https://blog.csdn.net/abc_12366/article/details/79966055
* https://blog.csdn.net/theonegis/article/details/50184811
* https://blog.csdn.net/hst_gogogo/article/details/82530929
